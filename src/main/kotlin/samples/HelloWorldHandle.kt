package samples

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
class HelloWorldHandle {

    // Endpoints/Routes - How other apps connects to the api
    @GetMapping("api/hello")
    fun hello(): String = "Hello World"

    @GetMapping("api/now")
    fun now(): String {
        return LocalDate.now().toString();
    }
}