package trakkr

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TrakkrApplication

fun main(args: Array<String>) {
	runApplication<TrakkrApplication>(*args)
}
