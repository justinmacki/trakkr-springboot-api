package trakkr.serviceImpl

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.User
import trakkr.repositories.UserRepository
import trakkr.service.UserService

@Service
class UserServiceImpl(
    private val repository: UserRepository
) : UserService {
    /**
     * CREATE A USER
     */
    override fun createUser(user: User): User {
        // save the user using the UserRepository
        return repository.save(user)
        /** LONG WAY
         * val newUser = repository.save(user)
         * return newUser
         */
    }

    /**
     * Get a specific user using an id
     */
    override fun getById(id: Long): User {
        // Find a user in the db and if it DNE, throw a 404 error
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
        }
        return user
    }

    override fun updateUser(body: User, id: Long): User {
        // 1. Find the entity first
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
        }

        // 2. Use save(). If the object does not exist yet in the db, it will insert the said object.
        // ELSE, it will update the object
        return repository.save(user.copy(
            firstName = body.firstName,
            lastName = body.lastName,
            email = body.email
        ))
    }

    override fun deleteUser(id: Long) {
        // 1. Find the entity
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
        }

        repository.delete(user)
    }

    override fun getAllUsers(): List<User> {
        // findAll() returns a Collection
        return repository.findAll().toList()
    }
}